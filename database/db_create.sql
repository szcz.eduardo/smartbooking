CREATE TABLE room (
    id SERIAL PRIMARY KEY,
    number INTEGER UNIQUE,
    floor INTEGER,
    capacity INTEGER,
    reserved BOOLEAN
);

CREATE TABLE reservation (
  id SERIAL PRIMARY KEY,
  room INTEGER REFERENCES room(id) ON DELETE CASCADE,
  occupation INTEGER,
  guest VARCHAR(255),
  breakfast BOOLEAN,
  car VARCHAR(50),
  checkin DATE,
  checkout DATE,
  status VARCHAR(50)
);

-- CREATE TABLE usuario (
--   id SERIAL PRIMARY KEY,
--   username VARCHAR(255),
--   password VARCHAR(255),
--   role VARCHAR(50)
-- );
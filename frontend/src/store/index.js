import ServiceAPI from '@/services/ServiceAPI'
import { createStore } from 'vuex'

export default createStore({
  state: {
    showSidebar: true,

    // alert
    alert: false,
    messageAlert: "",
    errorAlert: false,

    // infos
    availableRooms: 0,
    occupiedRooms: 0,
    totalGuests: 0,
    reservations: []
  },
  getters: {
  },
  mutations: {
    toggleSidebar(state) {
      state.showSidebar = !state.showSidebar
    },
    setAlert(state, data) {
      state.alert = data.alert
      state.messageAlert = data.messageAlert
      state.errorAlert = data.errorAlert
    },
    closeAlert(state) {
      state.alert = false
    },
    async refreshInfos(state) {
      await ServiceAPI.getReservations()
        .then((response) => {
          console.log(response.data)
          state.reservations = response.data
        })

      await ServiceAPI.getAvailableRooms()
        .then((response) => {
          state.availableRooms = response.data.length
        })
      
      await ServiceAPI.getRooms()
        .then((response) => {
          state.occupiedRooms = response.data.length - state.availableRooms
        })

      await ServiceAPI.getTotalGuests()
        .then((response) => {
          state.totalGuests = response.data
        })

    }
  },
  actions: {
  },
  modules: {
  }
})

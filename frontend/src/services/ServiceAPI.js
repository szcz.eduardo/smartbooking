import axios from 'axios'
import store from '../store/index';

const http = axios.create({
    baseURL: "http://localhost:8090/api",
    headers: {
        'Content-Type': 'application/json'
    }
})

const infoRoutes = ['http://localhost:8090/api/reservation', 'http://localhost:8090/api/room/available', 'http://localhost:8090/api/reservation/guests', 'http://localhost:8090/api/room']
http.interceptors.response.use((res) => {
    if (!infoRoutes.some(route => res.request.responseURL.includes(route))) {    
        store.commit('refreshInfos')
    }
    return res
})

class ServiceAPI {

    // Room
    getRooms() {
        return http.get('/room')
    }

    getRoomById(id) {
        return http.get(`/room/id/${id}`)
    }


    getRoomsByFloor(floor) {
        return http.get(`/room/floor/${floor}`)
    }

    getAvailableRooms() {
        return http.get('/room/available')
    }

    addRoom(data) {
        return http.post("/room", data)
    }

    // Reservation
    getReservations() {
        return http.get('/reservation')
    }

    getReservationsByStatus(status) {
        return http.get(`/reservation/status/${status}`)
    }

    setReservationStatus(id, status) {
        return http.put(`/reservation/status/${id}/${status}`)
    }

    addReservation(data) {
        return http.post('/reservation', data)
    }

    getTotalGuests() {
        return http.get('/reservation/guests')
    }

}

export default new ServiceAPI()
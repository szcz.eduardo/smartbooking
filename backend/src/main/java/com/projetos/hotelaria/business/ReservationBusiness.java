package com.projetos.hotelaria.business;

import com.projetos.hotelaria.db.DatabaseConnection;
import com.projetos.hotelaria.dto.ReservationDTO;
import com.projetos.hotelaria.dto.RoomDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ReservationBusiness {

    @Autowired
    private DatabaseConnection db;
    @Autowired
    private RoomBusiness roomBusiness;

    public List<ReservationDTO> getReservations() {
        String sql = "SELECT res.id, room.number as room, res.occupation, room.capacity, res.guest, res.breakfast, res.car, res.checkin, res.checkout, res.status FROM reservation res LEFT JOIN room room ON res.room = room.id";
        ResultSet rs = db.executeQuery(sql);
        return resultSetToReservationDTO(rs);
    }

    public List<ReservationDTO> getReservationsByStatus(String status) {
        String sql = String.format("SELECT res.id, room.number as room, res.occupation, room.capacity, res.guest, res.breakfast, res.car, res.checkin, res.checkout, res.status FROM reservation res LEFT JOIN room room ON res.room = room.id WHERE res.status = '%s'", status);
        ResultSet rs = db.executeQuery(sql);
        return resultSetToReservationDTO(rs);
    }

    public void setReservationStatus(Long id, String status) {
        String sql = String.format("UPDATE reservation SET status = '%s' WHERE id = %d", status, id);
        db.executeUpdate(sql);

        if (Objects.equals(status, "FINALIZADO")) {
            ResultSet rs = db.executeQuery(String.format("SELECT room FROM reservation WHERE id = %d", id));
            try {
                if (rs.next()) {
                    Long roomId = rs.getLong("room");
                    roomBusiness.setRoomReserved(roomId, false);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void addReservation(ReservationDTO reservationDTO) {
        Long room = reservationDTO.getRoom();
        Integer occupation = reservationDTO.getOccupation();
        String guest = reservationDTO.getGuest();
        Boolean breakfast = reservationDTO.getBreakfast();
        String car = reservationDTO.getCar();
        Date checkin = reservationDTO.getCheckin();
        Date checkout = reservationDTO.getCheckout();
        String status = "RESERVADO";

        String sql = String.format("INSERT INTO reservation (room, occupation, guest, breakfast, car, checkin, checkout, status) VALUES (%d, %d, '%s', '%s', '%s', '%s', '%s', '%s')",
                room, occupation, guest, breakfast, car, checkin, checkout, status);
        db.executeUpdate(sql);

        roomBusiness.setRoomReserved(room, true);
    }

    public Integer getTotalGuests() {
        String sql = "SELECT sum(occupation) as guests FROM reservation WHERE status = 'OCUPADO'";
        ResultSet rs = db.executeQuery(sql);

        try {
            if (rs.next()) {
                return rs.getInt("guests");
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<ReservationDTO> resultSetToReservationDTO(ResultSet rs) {
        List<ReservationDTO> reservationList = new ArrayList<>();
        try {
            while (rs.next()) {
                ReservationDTO reservation = new ReservationDTO();
                reservation.setId(rs.getLong("id"));
                reservation.setRoom(rs.getLong("room"));
                reservation.setOccupation(rs.getInt("occupation"));
                reservation.setCapacity(rs.getInt("capacity"));
                reservation.setGuest(rs.getString("guest"));
                reservation.setBreakfast(rs.getBoolean("breakfast"));
                reservation.setCar(rs.getString("car"));
                reservation.setCheckin(rs.getDate("checkin"));
                reservation.setCheckout(rs.getDate("checkout"));
                reservation.setStatus(rs.getString("status"));
                reservationList.add(reservation);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reservationList;
    }
}

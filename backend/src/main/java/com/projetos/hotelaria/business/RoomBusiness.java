package com.projetos.hotelaria.business;

import com.projetos.hotelaria.db.DatabaseConnection;
import com.projetos.hotelaria.dto.RoomDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoomBusiness {

    @Autowired
    private DatabaseConnection db;

    public List<RoomDTO> getRooms() {
        String sql = "SELECT * FROM room";
        ResultSet rs = db.executeQuery(sql);
        return resultSetToRoomDTO(rs);
    }

    public List<RoomDTO> getRoomById(Long id) {
        String sql = String.format("SELECT * FROM room WHERE id = %d", id);
        ResultSet rs = db.executeQuery(sql);
        return resultSetToRoomDTO(rs);
    }

    public List<RoomDTO> getRoomsByFloor(Integer floor) {
        String sql = String.format("SELECT * FROM room WHERE floor = %d", floor);
        ResultSet rs = db.executeQuery(sql);
        return resultSetToRoomDTO(rs);
    }

    public List<RoomDTO> getAvailableRooms() {
        String sql = "SELECT * FROM room WHERE reserved = false";
        ResultSet rs = db.executeQuery(sql);
        return resultSetToRoomDTO(rs);
    }
    
    public void addRoom(RoomDTO roomDTO) {
        Integer number = roomDTO.getNumber();;
        Integer floor = roomDTO.getFloor();
        Integer capacity = roomDTO.getCapacity();

        String sql = String.format("INSERT INTO room (number, floor, capacity, reserved) VALUES (%d, %d, %d, 'false')", number, floor, capacity);
        db.executeUpdate(sql);
    }

    public void setRoomReserved(Long id, Boolean reserved) {
        String sql = String.format("UPDATE room SET reserved = %s WHERE id = %d", reserved, id);
        db.executeUpdate(sql);
    }

    private static List<RoomDTO> resultSetToRoomDTO(ResultSet rs) {
        List<RoomDTO> roomList = new ArrayList<>();
        try {
            while (rs.next()) {
                RoomDTO room = new RoomDTO();
                room.setId(rs.getLong("id"));
                room.setNumber(rs.getInt("number"));
                room.setFloor(rs.getInt("floor"));
                room.setCapacity(rs.getInt("capacity"));
                room.setReserved(rs.getBoolean("reserved"));
                roomList.add(room);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roomList;
    }
}

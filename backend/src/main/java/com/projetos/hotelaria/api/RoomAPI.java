package com.projetos.hotelaria.api;

import com.projetos.hotelaria.business.RoomBusiness;
import com.projetos.hotelaria.dto.RoomDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/room", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:8080")
public class RoomAPI {

    @Autowired
    private RoomBusiness roomBusiness;

    @GetMapping
    @ResponseBody
    public List<RoomDTO> getRooms() {
        return roomBusiness.getRooms();
    }

    @GetMapping("/id/{id}")
    @ResponseBody
    public List<RoomDTO> getRoomById(@PathVariable("id") Long id) {
        return roomBusiness.getRoomById(id);
    }

    @GetMapping("/floor/{floor}")
    @ResponseBody
    public List<RoomDTO> getRoomsByFloor(@PathVariable("floor") Integer floor) {
        return roomBusiness.getRoomsByFloor(floor);
    }

    @GetMapping("/available")
    @ResponseBody
    public List<RoomDTO> getAvailableRooms() {
        return roomBusiness.getAvailableRooms();
    }

    @PostMapping
    @ResponseBody
    public void addRoom(@RequestBody RoomDTO roomDTO) {
        roomBusiness.addRoom(roomDTO);
    }

}


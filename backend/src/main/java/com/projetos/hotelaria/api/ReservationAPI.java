package com.projetos.hotelaria.api;

import com.projetos.hotelaria.business.ReservationBusiness;
import com.projetos.hotelaria.dto.ReservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/reservation", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:8080")
public class ReservationAPI {

    @Autowired
    private ReservationBusiness reservationBusiness;

    @GetMapping
    @ResponseBody
    public List<ReservationDTO> getReservations() {
        return reservationBusiness.getReservations();
    }

    @GetMapping("/status/{status}")
    @ResponseBody
    public List<ReservationDTO> getReservationsByStatus(@PathVariable("status") String status) {
        return reservationBusiness.getReservationsByStatus(status);
    }

    @PutMapping("/status/{id}/{status}")
    @ResponseBody
    public void setReservationStatus(@PathVariable("id") Long id, @PathVariable String status) {
        reservationBusiness.setReservationStatus(id, status);
    }

    @PostMapping
    @ResponseBody
    public void addReservation(@RequestBody ReservationDTO reservationDTO) {
        reservationBusiness.addReservation(reservationDTO);
    }

    @GetMapping("/guests")
    @ResponseBody
    public Integer getTotalGuests() {
        return reservationBusiness.getTotalGuests();
    }

}

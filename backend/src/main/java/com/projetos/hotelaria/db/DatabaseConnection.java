package com.projetos.hotelaria.db;

import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class DatabaseConnection {

    private static final String URL = "jdbc:postgresql://localhost:5432/hotelaria";
    private static final String USER = "root";
    private static final String PASSWORD = "123";

    private static Connection connect() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error to connect database!");;
            e.printStackTrace();
            return null;
        }
    }

    private static void disconnect(Connection connection) {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("Error to close connection!");
            e.printStackTrace();
        }
    }


    public ResultSet executeQuery(String sql) {
        try (Connection connection = connect()) {
            if (connection != null) {
                return connection.createStatement().executeQuery(sql);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            System.out.println("Error when executing query!");
            e.printStackTrace();
            return null;
        }
    }

    public void executeUpdate(String sql) {
        try (Connection connection = connect()) {
            if (connection != null) {
                connection.createStatement().executeUpdate(sql);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            System.out.println("Error when executing query!");
            e.printStackTrace();
        }
    }
}
